import { Component, OnInit } from '@angular/core';

import { ServersService } from '../servers.service';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {CanComponentDeactivate} from "./can-deactivate-guard.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-edit-server',
  templateUrl: './edit-server.component.html',
  styleUrls: ['./edit-server.component.css']
})
export class EditServerComponent implements OnInit, CanComponentDeactivate {
  server: {id: number, name: string, status: string};
  serverName = '';
  serverStatus = '';
  allowEdit = false;
  changeUpdated = false;

  constructor(private serversService: ServersService,
              private activatedRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    console.log(this.activatedRoute.snapshot.queryParams)
    console.log(this.activatedRoute.snapshot.fragment)

    this.activatedRoute.queryParams.subscribe((queryParams)=>{
      this.allowEdit = (queryParams['allowEdit'] === '1') ? true : false;
    })
    this.activatedRoute.params.subscribe((params: Params)=>{
      this.server = this.serversService.getServer(+this.activatedRoute.snapshot.params['id']);
    })
    const id = +this.activatedRoute.snapshot.params['id']
    this.server = this.serversService.getServer(id);
    this.serverName = this.server.name;
    this.serverStatus = this.server.status;
  }

  onUpdateServer() {
    this.serversService.updateServer(this.server.id, {name: this.serverName, status: this.serverStatus});
    this.changeUpdated = true;
    this.router.navigate(['../'], {relativeTo: this.activatedRoute});
  }

  canDeactivate(): Observable<boolean> |Promise<boolean> | boolean{
    if(!this.allowEdit){
      return true
    }
    console.log("edit can edit")
    if ((this.serverName !== this.server.name || this.serverStatus == this.server.status) && !this.changeUpdated  ) {
      return confirm('Do you want to Discard Changes?')
    }else {
      return true;
    }
  }


}
